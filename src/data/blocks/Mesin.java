package data.blocks;

import data.foundation.Vertex;

/**
 * class yang menyimpan nilai dari mesin seperti volume CC, dan tipe transmisi
 */
public class Mesin extends Vertex {
    public final int cc;
    public Mesin(int cc) {
        this.cc = cc;
    }

    @Override
    public String getValue() {
        return String.valueOf(cc);
    }

    @Override
    public String getValueWithUnit() {
        return super.getValueWithUnit() + " CC";
    }
}
