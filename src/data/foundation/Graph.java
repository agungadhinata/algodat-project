package data.foundation;

import data.blocks.Motor;

public class Graph {
    public Vertex root;

    // Main lists
    DoubleLinkedListVertex searchQueryList = new DoubleLinkedListVertex();
    DoubleLinkedListVertex visitedSearchQueryList = new DoubleLinkedListVertex();
    private DoubleLinkedListVertex visitedList = new DoubleLinkedListVertex();
    private DoubleLinkedListVertex queueList = new DoubleLinkedListVertex();
    private DoubleLinkedListVertex resultList = new DoubleLinkedListVertex();

    public Graph(Vertex root) {
        this.root = root;
        System.out.println("graph dengan root : " + root.getValue());
    }

    void resetVisitedListAndQueueList() {
        visitedList = new DoubleLinkedListVertex();
        visitedSearchQueryList = new DoubleLinkedListVertex();
        queueList = new DoubleLinkedListVertex();
    }

    public void resetAllList() {
        resetVisitedListAndQueueList();
        resetSearchList();
    }

    void resetSearchList() {
        resultList = new DoubleLinkedListVertex();
        searchQueryList = new DoubleLinkedListVertex();
    }

    // berfungsi untuk mencari vertex yang terdapat pada graph
    public Vertex getVertex(String tipeDataInString, String value) {
        tipeDataInString = tipeDataInString.toLowerCase();
        value = value.toLowerCase();
        resetVisitedListAndQueueList();
        queueList.add(root);
        Vertex current;
        while (!queueList.isEmpty()) {
            current = queueList.removeBack();
            //cek jika vertex tersebut telah dicari
            if (visitedList.peek(current) != null) continue;
            visitedList.add(current);
            if (current.getClass().getSimpleName().equalsIgnoreCase(tipeDataInString)) {
                if (current.getValue().equalsIgnoreCase(value)) {
                    return current;
                }
            }

            current.forEach(item -> {
                queueList.add(item);
            });
        }
        return null;
    }

//    public void printBFS() {
//        resetVisitedListAndQueueList();
//        Vertex current;
//        queueList.add(root);
//        while (!queueList.isEmpty()) {
//            current = queueList.removeBack();
//            if (visitedList.peek(current) != null) continue;
//            visitedList.add(current);
//            // do something
//            System.out.println(current.getClass().getSimpleName() + " : " + current.getValue());
//            current.forEach(item -> {
//                queueList.add(item);
//            });
//        }
//    }

    public void printBFSBySearchProps() {
        searchQueryList.forEach(atributSearch -> {
            // jika result kosong, dapatkan data dari root
            if (visitedSearchQueryList.peek(atributSearch) == null) {
                visitedSearchQueryList.add(atributSearch);
                if (resultList.isEmpty()) {
                    resetVisitedListAndQueueList();
                    queueList.add(root);
                    while (!queueList.isEmpty()) {
                        final Vertex current = queueList.removeBack();
                        if (visitedList.peek(current) != null) continue;
                        visitedList.add(current);
                        if (current instanceof Motor) {
                            // masukkan current ke result jika vertex propsItem pada vertex graph pada current adalah sama
                            current.forEach(atributCurrent -> {
                                if (atributCurrent == atributSearch) {
                                    // masukkan current hanya sebanyak sekali
                                    if (resultList.peek(current) == null) {
                                        resultList.add(current);
                                    }
                                }
                            });
                        }

                        // end do something
                        current.forEach(item -> {
                            queueList.add(item);
                        });
                    }
                }
                //jika result tidak kosong, maka gunakan linked list result dalam mencari properti yang serupa
                else {
                    DoubleLinkedListVertex tempResultList = new DoubleLinkedListVertex();
                    resultList.forEach(motor -> {
                        motor.forEach(atributMotor -> {
                            if (atributMotor == atributSearch) {
                                if (tempResultList.peek(motor) == null) {
                                    tempResultList.add(motor);
                                }
                            }
                        });
                    });
                    resultList = tempResultList;
                }
            }
        });
    }

    public void showSearchResult() {
        resultList.forEach(item -> {
            System.out.print(item.getValue() + ", ");
        });
        System.out.println("\n");
    }

    public void addOrderPencarian(String tipeData, String value) {
        searchQueryList.addBack(getVertex(tipeData, value));
    }
    public void printInlineSearchOrder() {
        searchQueryList.forEach(item-> System.out.print(item.getValue()+ " - "));
        System.out.println();
    }
    public void printMotorDetails() {
        resultList.forEach(motorVertex-> {
            System.out.println("Nama: " + motorVertex.getValue());
            motorVertex.forEach(attrib-> {
                System.out.println(" - " + attrib.getClass().getSimpleName() + " : " + attrib.getValueWithUnit());
            });
        });
    }
}
