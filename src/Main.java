import data.DataRepository;
import data.foundation.Graph;

import java.util.Scanner;

public class Main {
    public static void clearScreen() {
        for (int i = 0; i < 20; i++) {
            System.out.println();
        }
    }
    public static void main(String[] args) {
        Graph graphData = DataRepository.generateData();
        Scanner scan = new Scanner(System.in);
        System.out.println("==== SELAMAT DATANG ====");
        int inpuit;
        while (true) {
            inpuit = 0;
            clearScreen();
            System.out.print("key pencarian: ");
            graphData.printInlineSearchOrder();
            System.out.println("Pilihlah Kategori Motor dibawah ini: ");
            System.out.println("1. Berdasarkan Manufaktur\n2. Berdasarkan Warna\n3. Berdasarkan Ukuran \n4. Berdasarkan Transmisi\n---\n[8] reset | [9] Mulai mencari | [0] exit");
            System.out.print("> ");
            inpuit = scan.nextInt();
            scan.nextLine();
            if (inpuit == 1) {
                System.out.println("Pilih manufaktur: ");
                System.out.println("1. Honda\n2. yamaha");
                System.out.print("> ");
                inpuit = scan.nextInt();
                scan.nextLine();
                if (inpuit == 1) graphData.addOrderPencarian("manufaktur", "honda");
                if (inpuit == 2) graphData.addOrderPencarian("manufaktur", "yamaha");
                continue;
            } else if (inpuit == 2) {
                System.out.println("Pilih warna: ");
                System.out.println("1. Merah\n2. Putih\n3. Hitam\n4. Biru");
                System.out.print("> ");
                inpuit = scan.nextInt();
                scan.nextLine();
                if (inpuit == 1) graphData.addOrderPencarian("warna", "merah");
                if (inpuit == 2) graphData.addOrderPencarian("warna", "putih");
                if (inpuit == 3) graphData.addOrderPencarian("warna", "hitam");
                if (inpuit == 4) graphData.addOrderPencarian("warna", "biru");
                continue;
            } else if (inpuit == 3) {
                System.out.println("Pilih ukuran motor: ");
                System.out.println("1. kecil\n2. sedang \n3. besar");
                System.out.print("> ");
                inpuit = scan.nextInt();
                scan.nextLine();
                if (inpuit == 1) graphData.addOrderPencarian("ukuran", "kecil");
                if (inpuit == 2) graphData.addOrderPencarian("ukuran", "sedang");
                if (inpuit == 3) graphData.addOrderPencarian("ukuran", "besar");
                continue;
            } else if (inpuit == 4) {
                System.out.println("Pilih transmisi motor: ");
                System.out.println("1. manual\n2. auto");
                System.out.print("> ");
                inpuit = scan.nextInt();
                scan.nextLine();
                if (inpuit == 1) graphData.addOrderPencarian("transmisi", "manual");
                if (inpuit == 2) graphData.addOrderPencarian("transmisi", "auto");
                continue;
            } else if (inpuit == 8) {
                graphData.resetAllList();
                System.out.println("pencarian telah direset");
        } else if (inpuit == 9) {
                // proses pencarian
                graphData.printBFSBySearchProps();
                System.out.println("\nBerikut list motor sesuai dengan kategori pilihan anda: ");
                graphData.showSearchResult();

                System.out.println("Tampilkan detail? [1] yes [0] no");
                System.out.print("> ");
                inpuit = scan.nextInt();
                scan.nextLine();
                if(inpuit == 1) {
                    graphData.printMotorDetails();
                    System.out.println("Press enter key to continue... ");
                    scan.nextLine();
                }

                if (inpuit == 0) continue;
            } else if (inpuit == 0) {
                System.out.println("done");
                return;
            }
        }
    }
}